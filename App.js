import logo from './logo.svg';
import './App.css';
import React,{useState} from 'react';
function App() {

  const [firstName,setFirstName]=useState('');
  const [lastName,setLastName]=useState('');
  const [mobile , setMobile]=useState('');
  const [age,setAge]=useState('');
  const [email,setEmail]=useState('');
  const [password,setPassword]=useState('');

  function validateForm(){
    if(firstName.length==0){
      alert('Invalid Form')
      return
    }
    if(email.length==0){
      alert('Invalid Form')
      return
    }
    if(password.length==0){
      alert('Invalid Form')
      return
    }

    let countUpperCase=0;
    let countLowerCase=0;
    let countDigit=0;
    let countSpecialCharacter=0;

    for(let i=0;i<password.length;i++){
      const specialChars=[
        '!',
        '@',
        '$',
        '%',
        '^',
        '&',
        '*',
        '(',
        ')'

      ]

      if(specialChars.includes(password[i])){
        countSpecialCharacter++;
      }
      else if(!isNaN(password[i]*1)){
        countDigit++
      }
      else{
        if(password[i]==password[i].toUpperCase()){
          countUpperCase++
        }
        else{
          countLowerCase++
        }
      }
    }

    if(countUpperCase==0){
      alert('Invalide form')
    }
    
    if(countLowerCase==0){
      alert('Invalide form')
    }
    if(countDigit==0){
      alert('Invalide form')
    }
    if(countSpecialCharacter==0){
      alert('Invalide form')
    }
    alert('Form is Valid')
  };

  return (
    <div className="App">
      <input placeholder='Enter the First Name' onChange={(e)=>setFirstName(e.target.value)}></input>
      <input placeholder='Enter the Last name' onChange={(e)=>setLastName(e.target.value)}></input>
      <input placeholder='Entet the Mobile Number ' onChange={(e)=>setMobile(e.target.value)}></input>
      <input placeholder='Enter the Age ' onChange={(e)=>setAge(e.target.value)}></input>
      <input placeholder='Enter the Email ' onChange={(e)=>setEmail(e.target.value)}></input>
      <input placeholder='Enter the password' onChange={(e)=>setPassword(e.target.value)}></input>
      <button type='submit' onClick={()=>validateForm()}>Submit</button>
    </div>
  );
}

export default App;
